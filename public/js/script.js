console.log("hello there, General Kenobi");

const metaBtn = document.querySelector(".meta-btn");
const menu = document.querySelector(".header-navbar");

menu.addEventListener("click", (e) => {
    metaBtn.classList.remove("active");
    menu.classList.remove("active");
});

metaBtn.addEventListener("click", (e) => {
    metaBtn.classList.toggle("active");
    menu.classList.toggle("active");
    createOverlay();
});

function createOverlay() {
    if (metaBtn.classList.contains("active")) {
        const overlay = document.createElement("div");
        overlay.classList.add("overlay");
        document.body.append(overlay);
        overlay.addEventListener("click", (e) => {
            metaBtn.classList.remove("active");
            menu.classList.remove("active");
            overlay.remove();
        });
    }
};
const buttons = document.querySelectorAll(".github__btn");
buttons.forEach((btn) => btn.addEventListener("click", btnHandler));

function btnHandler(e) {
    const star = e.currentTarget.querySelector(".star>img");
    const starText = e.currentTarget.querySelector(".star>span");
    const counter = e.currentTarget.querySelector(".github__counter>a");
    let counterNum = parseInt(counter.innerText);
    if (!counter.classList.contains("clicked")) {
        counterNum++;
        counter.classList.add("clicked");
        if (star) {
            star.setAttribute("src", "./img/star-dark.svg");
            starText.innerHTML = "unstar";
        }
    } else {
        counterNum--;
        counter.classList.remove("clicked");
        if (star) {
            star.setAttribute("src", "./img/star.svg");
            starText.innerHTML = "star";
        }
    }
    counter.innerText = counterNum;
    console.log();
};
